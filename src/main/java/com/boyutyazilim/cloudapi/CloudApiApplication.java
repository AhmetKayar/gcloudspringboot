package com.boyutyazilim.cloudapi;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class CloudApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(CloudApiApplication.class, args);
	}
}